# F1 Champions

## Description

This small application represent simple Front-end development exercise with usage of various Front-end technologies, mostly based around Angular 6 framework.


## Workflow
On the home page, please choose the year of Formula 1 championship and next page will appear with detail report of all the race champions for that year. The champion for entire year will be highlighted and marked with racing flag.

## Logic
Since Ergast is a public API, the small caching mechanism has been implemented to store retrieved JSON data into the browser local storage. The logic is simply checking if is there a valid data in the browser cache that can be used. If not, then 2 parallel HTTP requests will be made to restart the cache process.

## Install

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: Node is been used as main engine for building application and starting of scripts.
2. [Yarn][]: Yarn is used as dependency management.
   
After installing Node and Yarn, you should be able to run the following command to install all dependencies and development tools.
You will only need to run this command when dependencies change in [package.json](package.json):

    yarn

## Run

Since the entire project has been build around [Angular 6][] framework, all scripts relay on Angular command line interface and ng commands.

In order to start and run the development environment, following command needs to be executed in root of the project:

    yarn start


## Unit tests

For running couple of example tests, please run command below:

    yarn test

For tests coverage support, please run:

    yarn coverage


## Code analyse

If you would like to check the code coverage and tests for any error, please run command below:

    yarn lint


## Build

To build entire project with production ready files, please run command below that will bundle everything and deploy it to 'dist' folder of project root:

    yarn build

## Browsers support
All modern desktop and mobile browsers are supported including IE11.


## List of frameworks and tools

* [Node.js](https://nodejs.org/) - JavaScript runtime built on Chrome's V8 JavaScript engine
* [Yarn](https://yarnpkg.com/) - Fast, reliable, and secure dependency management
* [Angular](https://angular.io/) - Angular framework
* [RxJS](https://rxjs-dev.firebaseapp.com/) - Reactive Extensions Library for JavaScript
* [NgRX](https://github.com/ngrx/platform) - Reactive libraries for Angular
* [Foundation](https://foundation.zurb.com/) - Foundation framework
* [Typescript](https://www.typescriptlang.org/) - TypeScript
* [Codelyzer](https://github.com/mgechev/codelyzer) - A set of tslint rules for static code analysis of Angular TypeScript projects.
* [TSLint](https://github.com/palantir/tslint) - TSLint is an extensible static analysis tool that checks TypeScript code
* [Pug](https://pugjs.org/) - Pug template engine
* [Karma](https://karma-runner.github.io/) - Productive testing environment
* [Jasmine](https://jasmine.github.io/) - Behavior-driven JS
* [Bourbon](https://www.bourbon.io/) - A lightweight Sass tool set
* [SASS](https://sass-lang.com/) - CSS extension language
* [PostCSS](https://postcss.org/) - Transforming CSS with JavaScript
* [Webpack](https://webpack.js.org/) - Static module bundler

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

[Node.js]: https://nodejs.org/
[Yarn]: https://yarnpkg.com/
[Angular 6]: https://angular.io/
