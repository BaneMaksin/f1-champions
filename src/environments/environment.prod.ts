/**
 * Production environment
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

export const environment = {
  production: true,
  ergastAPI: 'http://ergast.com/api/f1/'
};
