/**
 * Filter pipe
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Pipe, PipeTransform } from '@angular/core';

import { F1Data } from '../interfaces/response.interfaces';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  /**
   * Filter input data by search value
   *
   * @param {F1Data[]} value
   * @param {string} search
   * @returns {any}
   */
  transform(value: F1Data[], search: string): any {
    if (value && search) {
      search = search.toLowerCase();
      value = value.filter((data: F1Data) => {
        const {raceName, driverName, driverSurname, constructorName} = data;
        return raceName.toLowerCase().indexOf(search) !== -1 ||
          driverName.toLowerCase().indexOf(search) !== -1 ||
          driverSurname.toLowerCase().indexOf(search) !== -1 ||
          constructorName.toLowerCase().indexOf(search) !== -1;
      });
    }

    return value;
  }
}
