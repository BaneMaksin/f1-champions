/**
 * Range pipe
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { RangePipe } from './range.pipe';

describe('RangePipe', () => {
  it('create an instance', () => {
    const pipe = new RangePipe();
    expect(pipe).toBeTruthy();
  });

  it('should create array of length 9', () => {
    const pipe = new RangePipe();
    const range = pipe.transform([], 1, 10);
    expect(range.length).toEqual(9);
  });
});
