/**
 * Range pipe
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'range'
})
export class RangePipe implements PipeTransform {

  /**
   * Generate array based on input range
   *
   * @param _input
   * @param {number} min
   * @param {number} max
   * @param {number} step
   * @returns {number[]}
   */
  transform (
    _input: any,
    min: number,
    max: number,
    step: number = 1
  ): number[] {
    return Array.from(this._range(min, max, step));
  }

  /**
   * Range generator function
   *
   * @param {number} start
   * @param {number} end
   * @param {number} step
   * @returns {IterableIterator<number>}
   * @private
   */
  private* _range(start: number, end: number, step: number) {
    while (start < end) {
      yield start;
      start += step;
    }
  }
}
