/**
 * Filter pipe tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { FilterPipe } from './filter.pipe';

describe('FilterPipe', () => {
  it('create an instance', () => {
    const pipe = new FilterPipe();
    expect(pipe).toBeTruthy();
  });

  it('should filter input value', () => {
    const pipe = new FilterPipe();
    const filteredValue = pipe.transform([
      {
        raceName: 'Test',
        driverName: 'Test 2',
        driverSurname: 'Test 3',
        constructorName: 'Test 4',
        constructorURL: '#',
        lapsCount: '1',
        finishTime: '1',
        isSeasonChampion: true
      },
      {
        raceName: 'Example',
        driverName: 'Example 2',
        driverSurname: 'Example 3',
        constructorName: 'Example 4',
        constructorURL: '#',
        lapsCount: '1',
        finishTime: '1',
        isSeasonChampion: false
      }], 'test');
    expect(filteredValue.length).toEqual(1);
  });
});
