/**
 * SVG service tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import { of } from 'rxjs';

import { HeaderComponent } from '../components/header/header.component';
import { SvgDirective } from '../directives/svg.directive';
import { SvgService } from '../services/svg.service';

describe('Service: TooltipService', () => {
  let injector: TestBed;
  let service: SvgService;
  let httpMock: HttpTestingController;
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        HttpClientTestingModule
      ],
      declarations: [
        HeaderComponent,
        SvgDirective
      ],
      providers: [
        SvgService
      ]
    })
      .compileComponents();

    injector = getTestBed();
    service = injector.get(SvgService);
    httpMock = injector.get(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(service, 'getSVG').and.returnValue(of(
      document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    ));
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should make GET request and receive SVG element instance', async(() => {
    const mockResponse = '<svg xmlns="http://www.w3.org/2000/svg"/>';

    service.getSVG('flag.svg').subscribe((svg: SVGElement) => {
      expect(new XMLSerializer().serializeToString(svg)).toEqual(mockResponse);
    });

    const mockRequest = httpMock
      .expectOne((req: any): boolean => req.url.endsWith('.svg'));

    expect(mockRequest.request.method).toEqual('GET');

    // Satisfy the pending request in the mockHttp request queue
    mockRequest.flush(mockResponse);
  }));
});
