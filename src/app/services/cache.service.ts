/**
 * Cache service
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable } from '@angular/core';

import { F1Data, F1YearData } from '../interfaces/response.interfaces';

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  private _storageKey = 'f1Data';

  /**
   * Check do we have requested year in local browser cache
   *
   * @param year {Number} Requested year
   * @returns {boolean}
   */
  public check(year: string): boolean {

    // Check does our browser support local storage
    if (this._hasLocaleStorage) {
      const cacheData = JSON.parse(localStorage.getItem(this._storageKey)) || {};

      // Do we cached data
      return Object.keys(cacheData).length && cacheData[year];
    } else {
      return false;
    }
  }

  /***
   * Set current year data in local browser cache
   *
   * @param data {Object} Current year data
   */
  public set(data: F1YearData): void {
    let cacheData;

    // Check does our browser support local storage
    if (this._hasLocaleStorage) {
      cacheData = JSON.parse(localStorage.getItem(this._storageKey)) || {};

      // Check do we already have something cached
      cacheData[data.year] = data.data;
      localStorage.setItem(this._storageKey, JSON.stringify(cacheData));
    }
  }

  /**
   * Get data from local storage
   *
   * @returns {string}
   */
  public get(year: string): F1Data[] {
    return this._hasLocaleStorage ?
      JSON.parse(localStorage.getItem(this._storageKey))[year] :
      [];
  }

  /**
   * Check for storage support in current browser
   *
   * @returns {boolean}
   */
  private get _hasLocaleStorage(): boolean {
    const uid = new Date().getTime().toString();
    let storage,
        result;

    try {
      (storage = window.localStorage).setItem(uid, uid);
      result = storage.getItem(uid) === uid;
      storage.removeItem(uid);
      return result && storage;
    } catch (exception) {
      return false;
    }
  }
}
