/**
 * Cache service tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed, inject, async } from '@angular/core/testing';

import { CacheService } from './cache.service';

describe('CacheService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CacheService]
    });
  });

  it('should be created', inject([CacheService], (service: CacheService) => {
    expect(service).toBeTruthy();
  }));

  it('should set local storage value',
    inject([CacheService], (service: CacheService) => {
      service.set({
        year: '2011',
        data: [{
          raceName: 'Test',
          driverName: 'Test',
          driverSurname: 'Test',
          constructorURL: '#',
          constructorName: 'Test',
          lapsCount: '1',
          finishTime: '1',
          isSeasonChampion: true
        }]
      });

      expect(service.check('2011')).toBeDefined();
    })
  );

  it('should get valid local storage value',
    inject([CacheService], (service: CacheService) => {
      service.set({
        year: '2011',
        data: [{
          raceName: 'Test',
          driverName: 'Test',
          driverSurname: 'Test',
          constructorURL: '#',
          constructorName: 'Test',
          lapsCount: '1',
          finishTime: '1',
          isSeasonChampion: true
        }]
      });

      expect(service.get('2011')).toEqual([{
        raceName: 'Test',
        driverName: 'Test',
        driverSurname: 'Test',
        constructorURL: '#',
        constructorName: 'Test',
        lapsCount: '1',
        finishTime: '1',
        isSeasonChampion: true
      }]);
    })
  );
});
