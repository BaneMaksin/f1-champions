/**
 * Alert service tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed, inject } from '@angular/core/testing';

import { AlertService } from './alert.service';

describe('AlertService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertService]
    });
  });

  it('should be created', inject([AlertService], (service: AlertService) => {
    expect(service).toBeTruthy();
  }));

  it('should add new item to list', inject([AlertService], (service: AlertService) => {
    service.add({
      title: 'Test',
      message: 'Test'
    });

    expect(service.list).toEqual([{
      title: 'Test',
      message: 'Test'
    }]);
  }));
});
