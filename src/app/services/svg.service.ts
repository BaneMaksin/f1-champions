/**
 * SVG service
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { map, finalize, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SvgService {
  private static _inProgress: Map<string, Observable<SVGElement>> =
    new Map<string, Observable<SVGElement>>();
  private static _cache: Map<string, SVGElement> = new Map<string, SVGElement>();
  private _renderer: Renderer2;

  constructor(
    private _http: HttpClient,
    rendererFactory: RendererFactory2
  ) {
    this._renderer = rendererFactory.createRenderer(null, null);
  }

  /**
   * Get SVG file content
   *
   * @param url
   * @returns
   */
  getSVG(url: string): Observable<any> {

    // Check cache
    if (SvgService._cache.has(url)) {
      return of(SvgService._cache.get(url));
    }

    // Return existing fetch observable
    if (SvgService._inProgress.has(url)) {
      return SvgService._inProgress.get(url);
    }

    // Get the icon
    const request = this._http.get(url, {
      responseType: 'text'
    })
      .pipe(
        finalize(() => {
          SvgService._inProgress.delete(url);
        }),
        share(),
        map((svgText: string) =>
          SvgService._cache
            .set(url, this._svgElementFromString(svgText))
            .get(url)
        )
      );

    SvgService._inProgress.set(url, request);

    return request;
  }

  /**
   * Construct SVG element from string
   *
   * @param str
   * @returns
   */
  private _svgElementFromString(str: string): SVGElement | never {
    const wrapper = this._renderer.createElement('div');
    wrapper.innerHTML = str;
    return wrapper.querySelector('svg') as SVGElement;
  }
}
