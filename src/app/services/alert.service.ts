/**
 * Alert service
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable, EventEmitter } from '@angular/core';

import { Alert } from '../interfaces/alert.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public messageAdded$: EventEmitter<Alert>;

  private _alertList: Alert[] = [];

  /**
   * Class constructor
   * @constructor
   */
  constructor() {
    this.messageAdded$ = new EventEmitter();
  }

  /**
   * Get list of messages
   *
   * @returns {Alert[]}
   */
  public get list(): Alert[] {
    return this._alertList;
  }

  /**
   * Add message to list of alert messages
   *
   * @param {Alert} item
   */
  public add(item: Alert): void {
    this._alertList.push(item);
    this.messageAdded$.emit(item);
  }
}
