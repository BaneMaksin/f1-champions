/**
 * Main application component
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

import { HTTPStatus } from './interceptors/loader-interceptor.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.pug',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  HTTPActivity: boolean;

  private _unsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private _httpStatus: HTTPStatus
  ) {
    this._httpStatus.getHttpStatus()
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe(async (status: boolean) => {
        this.HTTPActivity = await status;
      });
  }

  /**
   * On component destroy lifecycle hook
   */
  ngOnDestroy(): void {
    this._unsubscribe$.next(true);
    this._unsubscribe$.complete();
  }
}
