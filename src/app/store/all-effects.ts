// -- IMPORT --
import { DataEffects } from './data/data.effects';

export const AllEffects = [
    // -- LIST --
    DataEffects
];
