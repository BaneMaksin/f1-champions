/**
 * Data store effects tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { ScannedActionsSubject, StoreModule } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { Observable, of, throwError } from 'rxjs';
import { cold } from 'jasmine-marbles';

import { reducers } from '../app.store';
import { DataEffects } from './data.effects';
import { DataService } from './data.service';
import {
  LoadDataAction,
  LoadDataSuccessAction,
  LoadDataFailAction
} from './data.actions';
import { DriverStandingsRaw, ResultsRaw } from '../../interfaces/response.interfaces';

describe('DataEffects', () => {
  let dataService: jasmine.SpyObj<DataService>;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      StoreModule.forRoot(reducers)
    ],
    providers: [
      DataEffects,
      Actions,
      ScannedActionsSubject,
      {
        provide: DataService,
        useValue: jasmine.createSpyObj('dataService', ['loadData'])
      }
    ]
  }));

  beforeEach(() => {
    dataService = TestBed.get(DataService);
  });

  describe('data$', () => {
    it('should return a LOAD_DATA_SUCCESS action, on success', () => {
      const year = '2011',
        resultsRaw: ResultsRaw = {
          MRData: {
            RaceTable: {
              Races: [],
              position: '',
              season: '',
            },
            limit: '',
            offset: '',
            series: '',
            total: '',
            url: '',
            xmlns: ''
          }
        },
        driverStandings: DriverStandingsRaw = {
          MRData: {
            StandingsTable: {
              StandingsLists: [],
              season: ''
            },
            limit: '',
            offset: '',
            series: '',
            total: '',
            url: '',
            xmlns: ''
          }
        },
          serviceResponse: [ResultsRaw, DriverStandingsRaw] = [resultsRaw, driverStandings];

      dataService.loadData.and.returnValue(of(serviceResponse));

      const actions: Observable<any> = cold('a', {
        a: new LoadDataAction(year)
      });

      const effects = new DataEffects(dataService, new Actions(actions));

      const expected = cold('b', {
        b: new LoadDataSuccessAction(serviceResponse)
      });

      expect(effects.loadData$).toBeObservable(expected);
    });

    it('should return a LOAD_DATA_FAIL action, on error', function () {
      const year = '2011',
            errorResponse: HttpErrorResponse = new HttpErrorResponse({
              error: 'Error thrown'
            });

      dataService.loadData.and.returnValue(throwError(errorResponse));

      const actions: Observable<any> = cold('a', {
        a: new LoadDataAction(year)
      });

      const effects = new DataEffects(dataService, new Actions(actions));

      const expected = cold('b', {
        b: new LoadDataFailAction(errorResponse)
      });

      expect(effects.loadData$).toBeObservable(expected);
    });
  });
});
