/**
 * Data store effects
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { DataService } from './data.service';
import * as dataActions from './data.actions';
import { DriverStandingsRaw, ResultsRaw } from '../../interfaces/response.interfaces';

@Injectable()
export class DataEffects {
  @Effect() loadData$;

  constructor(
    private dataService: DataService,
    private actions$: Actions
  ) {
    this.loadData$ = this.actions$
      .ofType(dataActions.LOAD_DATA)
      .pipe(switchMap((state: dataActions.LoadDataAction) =>
          this.dataService.loadData(state.payload).pipe(

            // If successful, dispatch success action with result
            map((res: [ResultsRaw, DriverStandingsRaw]) => new dataActions.LoadDataSuccessAction(res)),

            // If request fails, dispatch failed action
            catchError((err: Error) => of(new dataActions.LoadDataFailAction(err)))
          )
        )
      );
  }
}
