/**
 * Data store actions
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Action } from '@ngrx/store';

import { DriverStandingsRaw, ResultsRaw } from '../../interfaces/response.interfaces';

export const LOAD_DATA =                 '[Data] Load Data';
export const LOAD_DATA_SUCCESS =         '[Data] Load Data Success';
export const LOAD_DATA_FAIL =            '[Data] Load Data Fail';

/**
 * Load Data Actions
 */
export class LoadDataAction implements Action {
  readonly type = LOAD_DATA;

  constructor(public payload: string) { }
}

export class LoadDataSuccessAction implements Action {
  readonly type = LOAD_DATA_SUCCESS;

  constructor(public payload: [ResultsRaw, DriverStandingsRaw]) { }
}

export class LoadDataFailAction implements Action {
  readonly type = LOAD_DATA_FAIL;

  constructor(public error: Error) { }
}

export type Actions =
  | LoadDataAction
  | LoadDataSuccessAction
  | LoadDataFailAction;
