/**
 * Data store service
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
export class DataService {
  private _ergastAPI: string = environment.ergastAPI;

  constructor(
    private http: HttpClient
  ) {}

  /**
   * Make requests to retrieve data
   *
   * @param {string} year
   * @returns {Observable<Array<any>>}
   */
  loadData(year: string): Observable<Array<Object>> {
    return forkJoin([
      this.http.get(`${this._ergastAPI + year}/results/1.json`),
      this.http.get(`${this._ergastAPI + year}/driverStandings.json?limit=1`)
    ]);
  }
}
