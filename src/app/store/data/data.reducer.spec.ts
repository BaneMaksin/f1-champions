/**
 * Data store reducer tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { HttpErrorResponse } from '@angular/common/http';
import { DataState, initialState, reducer } from './data.reducer';
import { LoadDataAction, LoadDataSuccessAction, LoadDataFailAction } from './data.actions';
import { DriverStandingsRaw, ResultsRaw } from '../../interfaces/response.interfaces';

let sampleState: DataState;

describe('CsrfTokenReducer', () => {
  beforeEach(() => {
    sampleState = {
      loading: false,
      result: [],
      error: null,
      type: ''
    };
  });

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = reducer(undefined, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('LOAD_DATA action', () => {
    it('should clear any previous errors', () => {
      const action = new LoadDataAction('2011');

      const prevState = {
        ...sampleState,
      };

      const result = reducer(prevState, action);
      expect(result).toEqual({
        ...sampleState,
        loading: true,
        type: '[Data] Load Data'
      });
    });
  });

  describe('LOAD_DATA_SUCCESS action', () => {
    it('should update token to payload and error to null', () => {
      const resultsRaw: ResultsRaw = {
        MRData: {
          RaceTable: {
            Races: [],
            position: '',
            season: '',
          },
          limit: '',
          offset: '',
          series: '',
          total: '',
          url: '',
          xmlns: ''
        }
      };

      const driverStandings: DriverStandingsRaw = {
        MRData: {
          StandingsTable: {
            StandingsLists: [],
            season: ''
          },
          limit: '',
          offset: '',
          series: '',
          total: '',
          url: '',
          xmlns: ''
        }
      };

      const results: [ResultsRaw, DriverStandingsRaw] = [resultsRaw, driverStandings],
        action = new LoadDataSuccessAction(results);

      const prevState: DataState = {
        ...initialState,
      };

      const updatedState: DataState = {
        ...initialState,
        result: results,
        type: '[Data] Load Data Success'
      };

      const result = reducer(prevState, action);
      expect(result).toEqual(updatedState);
    });
  });

  describe('LOAD_DATA_FAIL action', () => {
    it('should update error to error text from response', () => {
      const action = new LoadDataFailAction(
        new HttpErrorResponse({
          error: 'Error'
        })
      );

      const prevState: DataState = sampleState;

      const updatedState: DataState = {
        ...sampleState,
        error: new HttpErrorResponse({ error: 'Error' }),
        type: '[Data] Load Data Fail'
      };

      const result = reducer(prevState, action);
      expect(result).toEqual(updatedState);
    });
  });

});
