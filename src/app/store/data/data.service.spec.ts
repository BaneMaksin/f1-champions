/**
 * Data store service tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 20.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed, getTestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { DataService } from './data.service';
import { ResultsRaw, DriverStandingsRaw } from '../../interfaces/response.interfaces';

describe('Service: DataService', () => {
  let injector: TestBed;
  let service: DataService;
  let httpMock: HttpTestingController;

  const resultsRaw: ResultsRaw = {
      MRData: {
        RaceTable: {
          Races: [],
          position: '',
          season: '',
        },
        limit: '',
        offset: '',
        series: '',
        total: '',
        url: '',
        xmlns: ''
      }
    };

  const driverStandings: DriverStandingsRaw = {
      MRData: {
        StandingsTable: {
          StandingsLists: [],
          season: ''
        },
        limit: '',
        offset: '',
        series: '',
        total: '',
        url: '',
        xmlns: ''
      }
    };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: DataService,
          useValue: {
            loadData: () => of([resultsRaw, driverStandings])
          }
        }
      ]
    });
    injector = getTestBed();
    service = injector.get(DataService);
    httpMock = injector.get(HttpTestingController);

    spyOn(service, 'loadData').and.callThrough();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', inject([DataService], (dataService: DataService) => {
    expect(dataService).toBeTruthy();
  }));

  it('should call the Ergast API and get the response', async () => {
    service.loadData('2011').subscribe((response: [ResultsRaw, DriverStandingsRaw]): void => {
      expect(response.length).toEqual(2);
      expect(response[0]).toEqual(resultsRaw);
      expect(response[1]).toEqual(driverStandings);
    });

    expect(service.loadData).toHaveBeenCalled();
  });
});
