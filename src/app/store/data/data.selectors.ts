/**
 * Data store selectors
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromData from './data.reducer';

// Create selector
export const getDataState = createFeatureSelector<fromData.DataState>('data');

export const dataFromStore = createSelector(
  getDataState,
  (state: fromData.DataState) => state.result
);
