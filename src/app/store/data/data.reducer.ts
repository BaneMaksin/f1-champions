/**
 * Data store reducer
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 19.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import * as data from './data.actions';

export interface DataState {
  loading: boolean;
  entities: { [id: string]: any };
  result: any[];
  error: Error;
  type: string;
}


export const initialState: DataState = {
  loading: false,
  entities: {},
  result: [],
  error: null,
  type: ''
};

export function reducer(state = initialState, action: data.Actions): DataState {
  switch (action.type) {
    case data.LOAD_DATA: {
      return {
        ...state,
        loading: true,
        error: null,
        type: action.type
      };
    }

    case data.LOAD_DATA_SUCCESS: {
      return {
        ...state,
        result: action.payload,
        loading: false,
        error: null,
        type: action.type
      };
    }

    case data.LOAD_DATA_FAIL: {
      return {
        ...state,
        loading: false,
        error: action.error,
        type: action.type
      };
    }

    default: {
      return state;
    }
  }
}
