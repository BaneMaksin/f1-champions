/**
 * SVG directive
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Directive, ElementRef, Input, OnChanges, OnDestroy, OnInit, Renderer2, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';

import { SvgService } from '../services/svg.service';

@Directive({
  selector: '[appSvg]',
  providers: [SvgService]
})
export class SvgDirective implements OnInit, OnChanges, OnDestroy {
  @Input() appSvg: string;
  @Input() colorSvg: string;

  private _prevSVG: SVGElement;
  private _prevUrl: string;
  private _subscription: Subscription;

  constructor(
    private _el: ElementRef,
    private _svgService: SvgService,
    private _renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this._insertSVG();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['insertSVG']) {
      this._insertSVG();
    }

    if (changes['colorSVG']) {
      this._colorEl(this._prevSVG);
    }
  }

  ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  /**
   * Insert SVG content
   */
  private _insertSVG(): void {

    // Check if a URL was actually passed into the directive
    if (!this.appSvg) {
      this._fail('No SVG path detected');
      return;
    }

    if (this.appSvg !== this._prevUrl) {
      this._prevUrl = this.appSvg;

      // Fetch SVG via cache mechanism
      this._subscription = this._svgService.getSVG(this.appSvg)
        .subscribe(
          (svg: SVGElement) => this._svgSuccess(svg),
          (err: any) => this._fail(err)
        );
    }
  }

  /**
   * Success handler
   *
   * @param svg
   * @returns
   */
  private _svgSuccess(svg: SVGElement) {
    if (svg) {
      this._colorEl(svg);
      this._insertEl(svg);
    }
  }

  /**
   * Fill the SVG color
   *
   * @param svg
   */
  private _colorEl(svg: SVGElement): void {
    if (svg && this.colorSvg) {
      Array.from(svg.children).forEach((child: HTMLElement) =>
        child.style.fill = this.colorSvg);
    }
  }

  /**
   * Insert SVG element into DOM
   *
   * @param el
   */
  private _insertEl(el: Element): void {
    const parentNode = this._prevSVG && this._prevSVG.parentNode;
    if (parentNode) {
      this._renderer.removeChild(parentNode, this._prevSVG);
    }

    this._el.nativeElement.innerHTML = '';
    this._renderer.appendChild(this._el.nativeElement, el);

    if (el.nodeName === 'svg') {
      this._prevSVG = el as SVGElement;
    }
  }

  /**
   * Error handler
   *
   * @param errMsg
   * @returns
   */
  private _fail(errMsg?: string): void {
    throw errMsg;
  }
}
