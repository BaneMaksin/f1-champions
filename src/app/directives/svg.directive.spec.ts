/**
 * SVG directive tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {HttpClientTestingModule } from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HeaderComponent } from '../components/header/header.component';
import { SvgDirective } from './svg.directive';
import { SvgService } from '../services/svg.service';

describe('Directive: SvgDirective', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        HeaderComponent,
        SvgDirective
      ],
      providers: [
        SvgService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should render element and have valid svg input property', async(() => {
    const directiveEl = fixture.debugElement.query(By.directive(SvgDirective));
    expect(directiveEl).not.toBeNull();

    const directiveInstance = directiveEl.injector.get(SvgDirective);
    expect(directiveInstance.appSvg).toBe('assets/images/icons/formula-car.svg');
  }));
});
