/**
 * Loader interceptor service
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { catchError, finalize, map, share } from 'rxjs/operators';

@Injectable()
export class HTTPStatus {
  private _requestInFlight$: BehaviorSubject<boolean>;

  constructor() {
    this._requestInFlight$ = new BehaviorSubject(false);
  }

  /**
   * Set status
   *
   * @param {boolean} inFlight
   */
  setHttpStatus(inFlight: boolean): void {
    this._requestInFlight$.next(inFlight);
  }

  /**
   * Get status
   *
   * @returns {Observable<boolean>}
   */
  getHttpStatus(): Observable<boolean> {
    return this._requestInFlight$.asObservable();
  }
}

@Injectable()
export class HTTPListener implements HttpInterceptor {
  constructor(
    private _status: HTTPStatus
  ) {}

  /**
   * Interceptor
   *
   * @param {HttpRequest<any>} req
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<any>>}
   */
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<any> {
    return next.handle(req).pipe(
      map(event => {
        this._status.setHttpStatus(true);
        return event;
      }),
      share(),
      catchError(error => throwError(error)),
      finalize(() => {
          this._status.setHttpStatus(false);
      })
    );
  }
}
