/**
 * Loader interceptor service tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed, inject } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { HTTPListener, HTTPStatus } from '../interceptors/loader-interceptor.service';

const RxJS_Services = [HTTPListener, HTTPStatus];

describe('CrsfInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ...RxJS_Services,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HTTPListener,
          multi: true
        }
      ]
    });
  });

  it('should be created', inject([HTTPListener], (service: HTTPListener) => {
    expect(service).toBeTruthy();
  }));
});
