/**
 * Error component
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Component } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.pug',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent {

  /**
   * Go back to previous page
   */
  goBack(): void {
    window.history.back();
  }
}
