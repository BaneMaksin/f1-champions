/**
 * Drivers component
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { AlertService } from '../../services/alert.service';
import { CacheService } from '../../services/cache.service';
import {
  DataRaw,
  DriverStandings,
  DriverStandingsRaw,
  F1YearData,
  RaceTable,
  ResultsRaw
} from '../../interfaces/response.interfaces';
import { RouteParams } from '../../interfaces/route.interfaces';

import { DataState } from '../../store/data/data.reducer';
import { LoadDataAction } from '../../store/data/data.actions';
import { dataFromStore } from '../../store/data/data.selectors';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.pug',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit, OnDestroy {
  public f1YearData: F1YearData;
  public search: string;

  private _unsubscribe$: Subject<boolean> = new Subject<boolean>();
  private _dataRaw: DataRaw;

  constructor(
    private _store: Store<DataState>,
    private _cache: CacheService,
    private _route: ActivatedRoute,
    private _alert: AlertService
  ) {
    this.f1YearData = {
      year: '',
      data: []
    };

    this._dataRaw = {
      results: []
    };
  }

  /**
   * On initialization lifecycle hook
   */
  ngOnInit() {

    // Start listening for data changes from store
    this._store
      .select(dataFromStore)
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe(
        this._success.bind(this),
        this._error.bind(this)
      );

    // Get year from URL params
    this._route.params
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe(
        (param: RouteParams) => this._check(param.year),
        this._error
      );
  }

  /**
   * On component destroy lifecycle hook
   */
  ngOnDestroy(): void {
    this._unsubscribe$.next(true);
    this._unsubscribe$.complete();
  }

  /**
   * Check do we have data in cache for selected year
   *
   * @param {string} year
   * @private
   */
  private _check(year: string): void {
    this.f1YearData.year = year;

    // Check cache data
    if (this._cache.check(year)) {
      this.f1YearData.data = this._cache.get(year);
    } else {
      this._store.dispatch(new LoadDataAction(year));
    }
  }

  /**
   * Successful handler for API data request
   *
   * @param {[ResultsRaw , DriverStandingsRaw]} results
   * @private
   */
  private _success(results: [ResultsRaw, DriverStandingsRaw]) {
    if (results.length) {

      // Verify response data
      if (this._verifyData(results[0], results[1])) {
        this._formatData();
      } else {
        this._error({
          name: 'Verification failed',
          message: 'Data verification failed, please try again'
        });
      }

      // Finally cache the data
      if (this.f1YearData.data.length) {
        this._cache.set(this.f1YearData);
      }
    }
  }

  /**
   * Verify raw data
   *
   * @param {ResultsRaw} raceTable
   * @param {DriverStandingsRaw} driverStandings
   * @returns {boolean}
   * @private
   */
  private _verifyData(raceTable: ResultsRaw, driverStandings: DriverStandingsRaw): boolean {
    if (typeof raceTable !== 'undefined' && raceTable.hasOwnProperty('MRData') &&
      raceTable.MRData.hasOwnProperty('RaceTable') &&
      raceTable.MRData.RaceTable.hasOwnProperty('Races')) {
        this._dataRaw.results = raceTable.MRData.RaceTable.Races;
    } else {
      return false;
    }

    if (typeof driverStandings !== 'undefined' && driverStandings.hasOwnProperty('MRData') &&
      driverStandings.MRData.hasOwnProperty('StandingsTable') &&
      driverStandings.MRData.StandingsTable.hasOwnProperty('StandingsLists') &&
      driverStandings.MRData.StandingsTable.StandingsLists.length &&
      driverStandings.MRData.StandingsTable.StandingsLists[0].hasOwnProperty('DriverStandings') &&
      driverStandings.MRData.StandingsTable.StandingsLists[0].DriverStandings.length &&
      driverStandings.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].hasOwnProperty('Driver')) {
        this._dataRaw.standings = driverStandings.MRData.StandingsTable.
          StandingsLists[0].DriverStandings[0].Driver;
    } else {
      return false;
    }

    return true;
  }

  /**
   * Format raw data for template view
   *
   * @private
   */
  private _formatData() {
    const results = this._dataRaw.results,
          standings = this._dataRaw.standings;

    // Check for session Champion
    if (results.length) {

      this.f1YearData.data = results.reduce(
        (accumulator, currentValue, index) => {

          if (currentValue.hasOwnProperty('Results') && currentValue.Results.length &&
            currentValue.Results[0].hasOwnProperty('Driver') &&
            currentValue.Results[0].Driver.hasOwnProperty('driverId') &&
            standings.hasOwnProperty('driverId')) {
            const result = currentValue.Results[0];

            accumulator[index] = {
              raceURL: currentValue.url ? currentValue.url : '#',
              raceName: currentValue.raceName ? currentValue.raceName : '/',
              driverName: result.Driver.givenName ? currentValue.Results[0].Driver.givenName : '',
              driverSurname: result.Driver.familyName,
              constructorURL: result.Constructor.url ? currentValue.Results[0].Constructor.url : '#',
              constructorName: result.Constructor.name ? currentValue.Results[0].Constructor.name : '/',
              lapsCount: result.laps ? currentValue.Results[0].laps : '/',
              finishTime: result.Time ? currentValue.Results[0].Time.time : '/',
              isSeasonChampion: result.Driver.driverId === standings.driverId
            };
          }

          return accumulator;
        }, []
      );
    } else {
      this._error({
        name: 'Format error',
        message: 'Data format failed, please try again'
      });
    }
  }

  /**
   * Authentication error handler
   *
   * @param {Error | HttpErrorResponse} error
   * @private
   */
  private _error(error: Error | HttpErrorResponse): void {

    // Server or connection error happened
    if (error instanceof HttpErrorResponse) {
      this._alert.add({
        title: error.statusText,
        message: error.message
      });
    } else {
      this._alert.add({
        title: error.name,
        message: error.message
      });
    }
  }
}
