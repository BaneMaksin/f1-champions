/**
 * Drivers component tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { of } from 'rxjs';

import { reducers } from '../../store/app.store';
import { DriversComponent } from './drivers.component';
import { SvgDirective } from '../../directives/svg.directive';
import { FilterPipe } from '../../pipes/filter.pipe';

describe('DriversComponent', () => {
  let component: DriversComponent;
  let fixture: ComponentFixture<DriversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientTestingModule,
        StoreModule.forRoot(reducers)
      ],
      declarations: [
        DriversComponent,
        SvgDirective,
        FilterPipe
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              year: '2011'
            })
          },
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
