/**
 * Alert component tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { AlertComponent } from './alert.component';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call close click event method', async(() => {
    spyOn(component, 'close');

    component['alerts'] = [{
      title: 'Test',
      message: 'Test'
    }];

    fixture.detectChanges();

    fixture.debugElement.query(
      By.css('.close-button')
    ).nativeElement.click();

    fixture.whenStable().then(() => {
      expect(component.close).toHaveBeenCalled();
    });
  }));
});
