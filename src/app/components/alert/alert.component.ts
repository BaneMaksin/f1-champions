/**
 * Alert component
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Component, Input } from '@angular/core';

import { Alert } from '../../interfaces/alert.interfaces';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.pug',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent {
  @Input() alerts: Alert[];

  /**
   * Class constructor
   * @constructor
   */
  constructor(
    private alertService: AlertService
  ) {
    this.alerts = alertService.list;
  }

  /**
   * Reset and close messages
   */
  close(): void {
    this.alerts.length = 0;
  }
}
