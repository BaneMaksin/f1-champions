/**
 * Drivers guard tests
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed, inject } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

import { DriversGuard } from './drivers.guard';

describe('DriversGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [DriversGuard]
    });
  });

  it('should create drivers guard', inject([DriversGuard], (guard: DriversGuard) => {
    expect(guard).toBeTruthy();
  }));
});
