/**
 * Drivers guard
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DriversGuard implements CanActivate {
  constructor (
    private _router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const year = next.params.year,
          isNotValid = isNaN(year) || year < 2005 || year > 2015;

    if (isNotValid) {
      this._router.navigate(['error']);
    }

    return !isNotValid;
  }
}
