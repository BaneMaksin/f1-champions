/**
 * Tests for main application component
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AlertComponent } from './components/alert/alert.component';
import { RouterModule } from '@angular/router';
import { SvgDirective } from './directives/svg.directive';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { HTTPListener, HTTPStatus } from './interceptors/loader-interceptor.service';
import { HomeComponent } from './components/home/home.component';
import { DriversComponent } from './components/drivers/drivers.component';
import { ErrorComponent } from './components/error/error.component';
import { RangePipe } from './pipes/range.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { AppRoutingModule } from './app-routing.module';

const RxJS_Services = [HTTPListener, HTTPStatus];

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        RouterModule,
        AppRoutingModule
      ],
      declarations: [
        AppComponent,
        HomeComponent,
        DriversComponent,
        RangePipe,
        FilterPipe,
        HeaderComponent,
        ErrorComponent,
        SvgDirective,
        FooterComponent,
        AlertComponent
      ],
      providers: [
        ...RxJS_Services,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HTTPListener,
          multi: true
        },
        {
          provide: APP_BASE_HREF,
          useValue : '/'
        }
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
