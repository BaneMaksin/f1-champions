/**
 * Response interfaces
 * Author: Branislav Maksin, branislav.maksin@gecko.rs
 * Date: 18.6.2018
 * Copyright: MIT (c) 2018 Branislav Maksin
 * Version: 1.0.0
 */

export interface RaceTableBasic {
  date: string;
  raceName: string;
  season: string;
  time: string;
  url: string;
}

export interface RaceTableCircuit extends RaceTableBasic {
  Circuit: {
    Location: {
      country: string;
      lat: string;
      locality: string;
      long: string;
    },
    circuitId: string;
    circuitName: string;
    url: string;
  };
}

export interface RaceTableResults {
  Constructor: any;
  Driver: any;
  FastestLap: any;
  Time: any;
  grid: string;
  laps: string;
  number: string;
  points: string;
  position: string;
  positionText: string;
  status: string;
}

export interface RaceTable extends RaceTableCircuit {
  Results: RaceTableResults[];
}

export interface Driver {
  code: string;
  dateOfBirth: string;
  driverId: string;
  familyName: string;
  givenName: string;
  nationality: string;
  permanentNumber: string;
  url: string;
}

export interface DataRaw {
  results: RaceTable[];
  standings?: Driver;
}

export interface F1Data {
  raceName: string;
  driverName: string;
  driverSurname: string;
  constructorURL: string;
  constructorName: string;
  lapsCount: string;
  finishTime: string;
  isSeasonChampion: boolean;
}

export interface F1Year {
  year: string;
}

export interface F1YearData extends F1Year {
  data: F1Data[];
}

export interface ResultsRaw {
  MRData: {
    RaceTable: {
      Races: RaceTable[];
      position: string;
      season: string;
    };
    limit: string;
    offset: string;
    series: string;
    total: string;
    url: string;
    xmlns: string;
  };
}

export interface DriverStandings {
  Constructors: any;
  Driver: Driver;
  points: string;
  position: string;
  positionText: string;
  wins: string;
}

export interface StandingsLists {
  DriverStandings: DriverStandings[];
  round: string;
  season: string;
}

export interface DriverStandingsRaw {
  MRData: {
    StandingsTable: {
      StandingsLists: StandingsLists[];
      season: string;
    };
    limit: string;
    offset: string;
    series: string;
    total: string;
    url: string;
    xmlns: string;
  };
}
